const initialState = []; 

export const todos = (state = initialState, action) => {
    switch(action.type) {
        case "ADD_TODO" : return [action.data, ...state];
        case "CHANGE_STATUS" : {
            const task = action.data.task;
            const status = action.data.status;
            const currState = state;
            const currTask = currState.filter(todo => todo === task);
            currTask[0].status = status;
            return currState.filter(todo => todo !== task).concat(currTask[0]);
        }
        default : return state;
    }
}