import './App.css';
import { Col, Row } from 'react-bootstrap';
import 'bootstrap/dist/css/bootstrap.min.css';
import CreateTodo from './components/CreateTodo';
import ListItem from './components/ListItem';
import { useDrop } from 'react-dnd';
import { ItemTypes } from './utils/Constants';
import { Color } from './utils/Colors';
import { useSelector, useDispatch } from 'react-redux';
import { addTodo, changeTodoStatus } from './actions';

function App() {
  const dispatch = useDispatch();
  const todos = useSelector((state) => state.todos);

    // adding the new todo
    const addTodos = todo => {
        dispatch(addTodo(todo));
    }

    const changeStatus = (task, status) => {
      dispatch(changeTodoStatus({task: task, status: status}));
    }

    const [{isOverOpen}, dropOpen] = useDrop({
      accept: ItemTypes.CARD,
      drop: (item, monitor) => changeStatus(item, ItemTypes.OPEN),
      collect: monitor => ({
        isOverOpen: !!monitor.isOver()
      })
    })

    const [{isOverProgress}, dropProgress] = useDrop({
      accept: ItemTypes.CARD,
      drop: (item, monitor) => changeStatus(item, ItemTypes.PROGRESS),
      collect: monitor => ({
        isOverProgress: !!monitor.isOver()
      })
    })

    const [{isOverClose}, dropDone] = useDrop({
      accept: ItemTypes.CARD,
      drop: (item, monitor) => changeStatus(item, ItemTypes.CLOSE),
      collect: monitor => ({
        isOverClose: !!monitor.isOver()
      })
    })

  return (
    <div className="App">
      <header className="App-header" style={{backgroundColor: Color.GREY}}> 
        <CreateTodo onSubmit={addTodos}/>
        <Row style={{flexDirection: "row", width: 1000, margin: 10}}>
          <Col ref={dropOpen} className="Box" style={{backgroundColor: (isOverOpen ? Color.RED : Color.LIGHTRED)}}>
          <h3 style={{color: Color.GREY}}>Open</h3>
          {todos.filter(todo => todo.status === ItemTypes.OPEN)
                .map(todo => (
                  <ListItem todo={todo}/>
                ))
          }
          </Col>
          <Col ref={dropProgress} className="Box" style={{backgroundColor: (isOverProgress ? Color.YELLOW : Color.LIGHTYELLOW)}}>
          <h3 style={{color: Color.GREY}}>In Progress</h3>
          {todos.filter(todo => todo.status === ItemTypes.PROGRESS)
                .map(todo => (
                  <ListItem todo={todo}/>
                ))
          }
          </Col>
          <Col ref={dropDone} className="Box" style={{backgroundColor: (isOverClose ? Color.GREEN : Color.LIGHTGREEN)}}>
          <h3 style={{color: Color.GREY}}>Completed</h3>
          {todos.filter(todo => todo.status === ItemTypes.CLOSE)
                .map(todo => (
                  <ListItem todo={todo}/>
                ))
          }
          </Col>
        </Row>
      </header>
    </div>
  );
}

export default App;
