export const addTodo = (data) => {
    return {
        type : 'ADD_TODO',
        data : data
    }
}

export const changeTodoStatus = (data) => {
    return {
        type : 'CHANGE_STATUS',
        data : data
    }
}