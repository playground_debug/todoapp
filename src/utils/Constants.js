export const ItemTypes = {
    CARD: 'card',
    OPEN: 'open',
    PROGRESS: 'progress',
    CLOSE: 'close'
} 