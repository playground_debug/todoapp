export const Color = {
    GREY: '#282c34',
    RED : 'red',
    YELLOW: 'yellow',
    GREEN: 'green',
    LIGHTRED: '#ffadad',
    LIGHTYELLOW: '#ffffcf',
    LIGHTGREEN: '#a4f5e5'
} 