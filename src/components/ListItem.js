import React from 'react'
import { Card } from 'react-bootstrap'
import { useDrag } from 'react-dnd'
import { Color } from '../utils/Colors'
import { ItemTypes } from '../utils/Constants'
import './ListItem.css'

function ListItem(props) {

    const[{opacity}, drag] = useDrag({
        type: ItemTypes.CARD,
        item: props.todo,
        collect: monitor => ({
            opacity: !!monitor.isDragging() ? 0.5 : 1
        })
    })
    return (
        <div ref={drag} style={{opacity}}>
           <Card className="Item" style={{color: Color.GREY}}>
            <Card.Text>
              {props.todo.data}
            </Card.Text>
        </Card> 
        </div>
    )
}

export default ListItem