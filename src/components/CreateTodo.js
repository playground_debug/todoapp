import React, { useState } from 'react'
import { Button, Form, Row, Col } from 'react-bootstrap'
import { ItemTypes } from '../utils/Constants';

function CreateTodo(props) {

    const [input, setInput] = useState('');

    const handleChange = input => {
        setInput(input.target.value);
    }

    const handleSubmit = e => {
        e.preventDefault();
        if(input === '') return;
        props.onSubmit({
            data : input,
            status: ItemTypes.OPEN
        })
        setInput('');
    };

    return (
        <div>
            <Form style={{ width: '40rem'}} onSubmit={handleSubmit}>
                <Row>
                    <Col>
                        <Form.Control
                        placeholder="Enter task here..."
                        onChange={handleChange}
                        value={input}
                        />
                    </Col>
                    <Col lg="3">
                        <Button type="submit" >Add Todo</Button>
                    </Col>
                </Row>
            </Form>
        </div>
    )
}

export default CreateTodo
